@extends('layouts.main')

@section('home')

<!--Hero-->
<div class="align-items-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-5 align-items-center" style="padding-top: 240px;">
          <h1 class="text-left">Hai, Saya Adhi Widhi</h1>
          <h2>Saya seorang Mahasiswa, Gamer, dan Editor</h2>       
          
        </div>
        <div class="col-lg-6"> <img src="{{asset('ui/img/20200623_174139.png')}}" class="pe-5 img-fluid " alt="Maudy Ayunda" ">
        </div>
      </div>
    </div>
  </div>
  <!--Akhir Hero-->

<!-- Biodata -->
 <section id=" Biodata">
    <div class="container">
      <div class="row" style="padding-top: 50px; padding-bottom: 50px;">
        <div class="col">
          <div class="section-title">
            <span>Biodata</span>
            <h2>Biodata</h2>
          </div>
        </div>
        <div class="container">
          <div class="row ">
            <div class="col-lg-4">
              <img src="{{ asset('ui/img/IMG-20191122-WA0008.jpg') }}"
                class="img-fluid" alt="Maudy Ayunda" width="360">
            </div>
            <div class="col content">
              <h3>Code Enthusiast & Editor</h3>
              <p class="fst-italic">
                Saya Adhi Widhi atau yang lebih dikenal dengan P.A adalah orang yang penggila coding dan editing.
              </p>
              <div class="row">
                <div class="col-lg-6">
                  <ul>
                    <li><strong>Tanggal Lahir:</strong> 26 September 2001 </li>
                    <li><strong>Website:</strong> www.belumpunya.com</li>
                    <li><strong>Kota:</strong> Singaraja, Indonesia</li>
                    <li><strong>Orang Tua :</strong> Kadek Widyartawan (Ayah)
                      Ni Ketut Toya Asih (ibu)</li>
                  </ul>
                </div>
                <div class="col-lg-6">
                  <ul>
                    <li><strong>Umur:</strong> 20</li>
                    <li><strong>Almamater:</strong> SMKN 3 Singaraja</li>
                    <li><strong>Penghargaan:</strong>
                      Masih mencari</li>
                    <li><strong>Pekerjaan:</strong> Reseller & Joki</li>
                  </ul>
                </div>
              </div>
              <p>Saya suka teh hijau dan senyum lebar. Saya suka seafood terutama kepiting (dalam saus
                singaporean).
                Saya suka membaca di bawah selimut saat hujan. Saya suka bersenang-senang dalam keajaiban dan
                mimpi, dan
                tertawa bahkan ketika itu tidak lucu. Saya suka barang vintage dan warna pastel, burung hantu, dan
                membuat semuanya rapi. Saya suka kutipan dan ikat kepala yang canggih, gaun dan sepatu bot. Saya
                suka
                menulis lagu tentang hal-hal yang paling acak. Saya suka lagu cinta romantis yang cengeng dan cara
                mereka membuat saya merasa. Saya suka pegunungan dan pantai, dan (terkadang) tidak peduli dengan
                dunia.
              </p>
              <p> Saya menganggap diri saya sangat beruntung, dan saya selamanya bersyukur atas kehidupan, teman,
                orang
                tua, keluarga, yang telah Tuhan berikan kepada saya. Saat ini, saya melakukan hal-hal yang saya
                sukai
                dan cintai setiap menitnya. Ini dia untuk saat ini, tapi saya harap Anda akan terus mengikuti saya
                dalam
                perjalanan ini!</p>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
<!-- Biodata -->

<!--Pengalaman-->
<section id="Pengalaman" class="pengalaman shadow-sm">
  <div class="container mt-5">
    <div class="row">
      <div class="col">
        <div class="section-title">
          <span>Pengalaman</span>
          <h2>Pengalaman</h2>
        </div>
      </div>
    </div>
    <div class="row ">
      <div class="col-lg-6">
        <div class="pengalaman-item ">
          <h4>Magang</h4>
          <h5>2018 - 2019</h5>
          <ul>
            <li>Magang di CV. Artha Ganesha Jejaring Nusa</li>
          </ul>
        </div>
      </div>
      <div class="col">
          <div class="pengalaman-item ">
              <h4>Pendidikan</h4>
              <h5>SD - Sekarang</h5>
              <p>
              <ul>
                <li>SD : SDN 3 Banjar Jawa</li>
                <li>SMP : SMPN 6 Singaraja</li>
                <li>SMK : SMKN 3 Singaraja</li>
              </ul>
              </p>
            </div>
      </div>
  </div>


    </div>
  </div>
</section>
<!--Pengalaman-->
<!-- Galeri -->
<section id=Galeri>
  <div class="bg-light p-5">
    <div class="container text-center">
      <div class="section-title">
        <span>Galeri</span>
        <h2>Galeri</h2>
        <div class="container">
          <div class="row row-cols-1 row-cols-md-3 gy-4">

            <div class="foto">
              <div class="col themed-grid-col-">
                <div class="card" style="width: 9cm;">
                  <div class="card-body">
                    <a target="_blank"
                      href="https://asset-a.grid.id/crop/0x0:0x0/700x465/photo/2019/10/06/2428112856.jpg">
                      <img src="{{asset('ui/img/gold jett.jpg')}}"
                        class="card-img-top" height="180cm">
                  </div>
                  </a>
                </div>
              </div>
            </div>

            <div class="foto">
              <div class="col themed-grid-col">
                <div class="card" style="width: 9cm;">
                  <div class="card-body">
                    <a target="_blank"
                      href="https://cdn.popmama.com/content-images/post/20200506/maudy-ayunda-inspirasi-anak-a8dcfe383429b299877c0891d9d719a4_800x420.jpg">
                      <img
                        src="{{asset('ui/img/Untitled-1.jpg')}}"
                        class="card-img-top" class="img-fluid" height="180cm">
                  </div>
                  </a>
                </div>
              </div>
            </div>

            <div class="foto">
              <div class="col themed-grid-col">
                <div class="card" style="width: 10cm;">
                  <div class="card-body">
                    <a target="_blank"
                      href="https://selasar.co/assets/images/news/2020/07/5f003856a63c61593849942-viral.jpeg">
                      <img src="{{asset('ui/img/sage thumbnail.jpg')}}"
                        class="card-img-top" height="180cm">
                  </div>
                  </a>
                </div>
              </div>
            </div>

            <div class="foto">
              <div class="col themed-grid-col-">
                <div class="card bg-transparent" style="width: 9cm;">
                  <div class="card-body">
                    <a target="_blank" href="https://www.tagar.id/Asset/uploads2019/1593849225644-maudy-ayunda.jpg">
                      <img src="{{asset('ui/img/21.jpg')}}"
                        class="card-img-top" height="200cm">
                  </div>
                </div>
                </a>
              </div>
            </div>

            <div class="foto">
              <div class="col themed-grid-col-">
                <div class="card bg-transparent" style="width: 9cm;">
                  <div class="card-body">
                    <a target="_blank"
                      href="https://cdn1-production-images-kly.akamaized.net/qSjqBZ1O5GUHSiG52reErN6N06Y=/0x0:1920x1280/320x180/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/3005400/original/012870200_1577331139-IMG_4164.JPG">
                      <img
                        src="{{asset('ui/img/asda.jpg')}}"
                        class="card-img-top" height="200cm">
                  </div>
                  </a>
                </div>
              </div>
            </div>

            <div class="foto">
              <div class="col themed-grid-col-">
                <div class="card bg-transparent" style="width: 10cm;">
                  <div class="card-body">
                    <a target="_blank"
                      href="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcStTozYoW-GYoqW5OIMlS2ET0pqjspRG_KhBg&usqp=CAU">
                      <img
                        src="{{asset('ui/img/digital-digital-art-artwork-illustration-abstract-hd-wallpaper-preview.jpg')}}"
                        class="card-img-top" height="200cm">
                  </div>
                  </a>
                </div>
              </div>
            </div>

            <div class="foto">
              <div class="col themed-grid-col-">
                <div class="card bg-transparent" style="width: 9cm;">
                  <div class="card-body">
                    <a target="_blank"
                      href="http://www.trinityproduction.com/wp-content/uploads/2015/10/BAML2256-1024x683.jpg">
                      <img src="{{asset('ui/img/44790574_2329788693921118_5800534112997800446_n.jpg')}}"
                        height="190cm" class="card-img-top">
                  </div>
                  </a>
                </div>
              </div>
            </div>

            <div class="foto">
              <div class="col themed-grid-col-">
                <div class="card bg-transparent" style="width: 9cm;">
                  <div class="card-body">
                    <a target="_blank"
                      href="https://i.pinimg.com/originals/b4/90/8a/b4908a5f1236fb38400b78d15d32c73e.jpg">
                      <img src="{{asset('ui/img/1.jpg')}}"
                        class="card-img-top">
                  </div>
                  </a>
                </div>
              </div>
            </div>

            <div class="responsive">
              <div class="foto">
                <div class="col themed-grid-col-">
                  <div class="card bg-transparent" style="width: 10cm;">
                    <div class="card-body">
                      <a target="_blank"
                        href="https://cdn-image.hipwee.com/wp-content/uploads/2019/11/hipwee-UNSET-1.jpg">
                        <img src="https://wearesocial-net.s3.amazonaws.com/wp-content/uploads/2020/11/gamer_room.jpg"
                          class="card-img-top">
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Galeri -->
<!-- Kontak -->
<section id="Kontak">
  <div class="KontakTitle text-center mt-5">
    <h1 class="kontak">Kontak</h1>
  </div>
  <div class="container">
    <div class="row justify-content-evenly">
      <div class="col-4 text-start textKontak">
        <h1>Hiii... <i class="bi bi-emoji-smile fs-3"></i></h1>
        <h2>Tinggalkan Pesan</h2>
        <h4>Atau</h4>
        <h2>Ikuti Media Sosial Saya...</h2>
      </div>
      <div class="col-4">
        <form>
          <div class="mb-3">
            <label for="nama" class="form-label ">Nama</label>
            <input type="text" class="form-control" id="Nama" aria-describedby="nama">
          </div>
          <div class="mb-3">
            <label for="Email" class="form-label">Alamat Email</label>
            <input type="email" class="form-control" id="Email" placeholder="david@example.com">
          </div>
          <div class="mb-3">
            <label for="Pesan" class="form-label">Pesan</label>
            <textarea class="form-control" id="pesan" rows="3"></textarea>
          </div>
          <button type="submit" class="btn btn-sumbit " style="border-radius: 50px;">Submit</button>
        </form>
      </div>
    </div>
  </div>
  <div class=" container medsos ">
    <div class="row text-center">
      <div class="col-md-6 offset-md-3 logo">
        <a href="https://www.facebook.com/AyundaFazaMaudy/" target="_blank"
          title="Menuju Halaman Profil Facebook Maudy Ayunda"><i class=" logo bi bi-facebook"></i></a>
        <a href="https://twitter.com/maudyayunda" target="_blank"
          title=" logo Menuju Halaman Profil Twitter Maudy Ayunda"><i class=" logo bi bi-twitter"></i></a>
        <a href="https://www.instagram.com/maudyayunda/" target="_blank"
          title="Menuju Halaman Profil Instagram Maudy Ayunda"><i class="logo bi bi-instagram"></i></a>
      </div>
    </div>
  </div>
</section>
<!-- Kontak -->
@endsection